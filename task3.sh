#!/bin/bash
help() {
	echo "Please specify directory name to check if it exists."
	exit 1
}

DIRECTORY=$1

if [[ $DIRECTORY == "" ]]; then 
	help
fi

if ! [ -d $1 ];
then
	echo "Directory $1 does not exist."
	mkdir $1
	echo "$1 was created."
else 
	echo "Directory $1 already exists."
fi 