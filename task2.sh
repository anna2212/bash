#!/bin/bash
help() {
	echo "Please specify name of the file for writting result and names of the input files."
	exit 1
}

RESULT=$1
FIRST_FILE=$2
SECOND_FILE=$3

if [[ $RESULT == "" || $FIRST_FILE == "" || $SECOND_FILE == "" ]]; then 
	help
fi

cat $FIRST_FILE $SECOND_FILE > $RESULT
echo "The files $FIRST_FILE and $SECOND_FILE were united and written to $RESULT."