#!/bin/bash
help() {
	echo "Please specify directory name to archive as an argument."
	exit 1
}

DIRECTORY=$1

if [[ $DIRECTORY == "" ]]; then 
	help
fi

if [ -d $DIRECTORY ]; then
	echo "archiving $DIRECTORY"
	tar -cvzf arch.tar.gz $DIRECTORY
fi

echo 'Choose an existing directory'